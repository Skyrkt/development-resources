# TODO: Name of project

TODO: Brief description of project

TODO Optional: Additional Information about the project

## Contributing

Skyrocket has written a comprehensive guide to get started with contributing to Skyrocket projects. You can view the document [here](https://app.tettra.co/teams/skyrocketdigital/pages/contributing-to-skyrocket-projects)

## Running the Project

TODO: Add here instructions that need to be run before `docker-compose up`. For example having to install yarn and run `yarn install`, or running `yarn start` for craft applications.

### Docker Compose
Once you have installed the needed dependancies as outline in the contributing section of this readme you can run the following bash command within this projects directory.
```bash
docker-compose up
```

From there visit the `docker-compose.yml` file that lives in the root of the project. This file outlines how this application will run in development mode. There will be a line that defines a environment variable named `VIRTUAL_HOST`. If you enter in the value of this variable into your browser it will direct to the development server for this applcation.

For more information on the VHOST configuration visit [dory's github page](https://github.com/FreedomBen/dory). And if you'd like to learn more about how docker-compose works read the docs [here](https://docs.docker.com/compose/).

## Troubleshooting

Skyrocket has put together a list of common problems that occur and their solutions [here](https://app.tettra.co/teams/skyrocketdigital/pages/engineering-faq).

TODO: Any other trouble shooting information
