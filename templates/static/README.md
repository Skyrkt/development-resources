# PHP7 Nginx Docker Template

Be sure to read the README.md [here](https://gitlab.com/Skyrkt/development-resources/tree/master/templates) on how to use this.

From [this docker container](https://hub.docker.com/r/skyrkt/nginx-node/)

### Good to know

* This is build with Yarn being using in mind, will fail without a yarn.lock file
* Written with a `yarn build` command assumed for building `/dist` and `yarn start` for development
