curl -sSL https://get.docker.com/ | sh
sudo usermod -aG docker ubuntu
sudo add-apt-repository ppa:certbot/certbot
sudo apt-get update
sudo apt-get install -y nginx certbot
sudo curl -o /usr/local/bin/docker-compose -L "https://github.com/docker/compose/releases/download/1.13.0/docker-compose-$(uname -s)-$(uname -m)"
sudo chmod +x /usr/local/bin/docker-compose
sudo reboot
