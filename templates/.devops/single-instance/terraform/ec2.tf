# A security group for the EB so it is accessible via the web
resource "aws_security_group" "ec2_security_group" {
  name        = "ec2_security_group"
  vpc_id      = "${aws_vpc.app.id}"

  ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    from_port   = 443
    to_port     = 443
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  # outbound internet access
  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}

resource "aws_instance" "app_production" {
    instance_type = "t2.micro"
    ami = "ami-f4cc1de2"
    subnet_id = "${aws_subnet.subnet_1.id}"

    key_name = "${var.application_info["key_pair"]}"
    vpc_security_group_ids = [ "${aws_security_group.ec2_security_group.id}" ]
    associate_public_ip_address = true

    tags {
        Name = "${var.application_info["name"]}"
    }
}

