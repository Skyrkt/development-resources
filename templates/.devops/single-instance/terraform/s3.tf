resource "aws_s3_bucket" "private_storage" {
    bucket = "${var.application_info["name_lower"]}-private-storage"
    acl = "private"
}

resource "aws_s3_bucket" "public_storage" {
    bucket = "${var.application_info["name_lower"]}-public-storage"
    acl = "public-read"
}
