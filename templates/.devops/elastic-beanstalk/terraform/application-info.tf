# Important! If you have cloned this repository there are two missing
# files which are required to successfully Terraform this project.
#
# Filename:
#   auth-for-container-registry
# Contents:
#   [contents are secret and found here](https://drive.google.com/file/d/0B8jhxIim8HV_M0xMSHFPUmZOS1k/view)
#
# Filename:
#   secrets.tf
# Contents:
#   variable "db_password" {
#     type = "map"
#     default = {
#       production = "something1",
#       staging = "something2"
#     }
#   }
#   variable "aws_access" {
#     type = "map"
#     default = {
#       access_key = "somethingkey"
#       secret_key = "somethingsecretkey"
#     }
#   }

variable "application_info" {
  type = "map"
  default = {
    name = "TODO_APP_NAME"
    name_lower = "TODO_APP_NAME_LOWER" // <-- No Spaces or dashes please
    key_pair = "TODO_APP_NAME_LOWER"
    main_domain = "TODO_APP_MAIN_DOMAIN"
  }
}
