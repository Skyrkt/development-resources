resource "aws_s3_bucket" "private_storage" {
    bucket = "${var.application_info["name_lower"]}-private-storage"
    acl = "private"
}

resource "aws_s3_bucket_object" "registry" {
    bucket = "${aws_s3_bucket.private_storage.bucket}"
    key = "auth-for-container-registry"
    source = "./auth-for-container-registry"
    acl = "private"
}

resource "aws_s3_bucket" "public_storage" {
    bucket = "${var.application_info["name_lower"]}-public-storage"
    acl = "public-read"
}
