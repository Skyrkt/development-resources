resource "aws_elastic_beanstalk_environment" "staging" {
  name = "${var.application_info["name_lower"]}-staging"
  application = "${aws_elastic_beanstalk_application.app.name}"
  template_name = "${aws_elastic_beanstalk_configuration_template.single_docker_template.name}"
  tier = "WebServer"
  cname_prefix = "${var.application_info["name_lower"]}-staging"

  # TODO This is an example of a environment variable you'll need to change this per your application
  # setting {
  #   namespace = "aws:elasticbeanstalk:application:environment"
  #   name      = "DB_HOST"
  #   value     = "${aws_db_instance.staging.endpoint}"
  # }
}

resource "aws_elastic_beanstalk_environment" "production" {
  name = "${var.application_info["name_lower"]}-production"
  application = "${aws_elastic_beanstalk_application.app.name}"
  template_name = "${aws_elastic_beanstalk_configuration_template.single_docker_template.name}"
  tier = "WebServer"
  cname_prefix = "${var.application_info["name_lower"]}-production"

  # TODO This is an example of a environment variable you'll need to change this per your application
  # setting {
  #   namespace = "aws:elasticbeanstalk:application:environment"
  #   name      = "DB_HOST"
  #   value     = "${aws_db_instance.staging.endpoint}"
  # }
}
