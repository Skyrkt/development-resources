resource "aws_route53_zone" "app" {
  name = "${var.application_info["main_domain"]}"
}

# TODO: Examples of common configuration, if this is for an existing project use the dns dig tool (link below) and replicate what you find here
# https://toolbox.googleapps.com/apps/dig/
# resource "aws_route53_record" "naked" {
#   zone_id = "${aws_route53_zone.app.zone_id}"
#   name = "${var.application_info["main_domain"]}"
#   type = "A"
#   ttl = "3600"
#   records = []
# }

# resource "aws_route53_record" "www" {
#    zone_id = "${aws_route53_zone.app.zone_id}"
#    name = "www.${var.application_info["main_domain"]}"
#    type = "CNAME"
#    ttl = "300"
#    records = ["${aws_route53_record.naked.name}"]
# }

# resource "aws_route53_record" "mail" {
#    zone_id = "${aws_route53_zone.app.zone_id}"
#    name = "${var.application_info["main_domain"]}"
#    type = "MX"
#    ttl = "3600"
#    records = ["1 ASPMX.L.GOOGLE.COM", "5 ALT1.ASPMX.L.GOOGLE.COM", "5 ALT2.ASPMX.L.GOOGLE.COM", "10 ALT3.ASPMX.L.GOOGLE.COM", "10 ALT4.ASPMX.L.GOOGLE.COM"]
# }

# resource "aws_route53_record" "txt" {
#    zone_id = "${aws_route53_zone.app.zone_id}"
#    name = "${var.application_info["main_domain"]}"
#    type = "TXT"
#    ttl = "3600"
#    records = ["google-site-verification=-LTxmsq2Kugqw3BcdVBJBZZZn899sr_udkrESLGhngg"]
# }
