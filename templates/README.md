# Templates

Look that the individual directories readmes for specific information.

### Every directory but .devops

These templates offer the files needed to "dockerize" a certain environment. They all use a common 'find and replace mechanism'

Just look for instances of TODO and replace matching ones with corrisponding data.

For example `TODO_APP_MAIN_DOMAIN` would get replaced with `example.com`.

### .devops directory

This directory includes files to add into your projects `.devops` directory.

### .gitlab.ci

The gitlab CI that should be put at the root of every project using the CI and elastic beanstalk. Be sure to replace TODO's appropriately

### docker-compose.yml

The `docker-compose.yml` in this directory is an examples of extending other docker-compose.yml files, this is useful when you follow Skyrocket's [submodule workflow](https://app.tettra.co/teams/skyrocketdigital/pages/submodule-workflow-for-decoupled-apps)
