# PHP7 Nginx Docker Template

Be sure to read the README.md [here](https://gitlab.com/Skyrkt/development-resources/tree/master/templates) on how to use this.

From [this docker container](http://dockerfile.readthedocs.io/en/latest/content/DockerImages/dockerfiles/php-nginx.html)

### Good to know

* This is build with Laravel 5 in mind so it composer installs, and also has some Laravel specific env vars in the `docker-compose.yml`
* The nginx files in this docker container are not stored in the usual spots.
* The VIRTUAL_HOST is prefixed with API
* assuming your files are in `/public`
