# Skyrocket Technical Exercise

At Skyrocket we have to be "Jack of all trades" so we've devised an excerise that hits lightly
on common tasks here at Skyrocket.

### Note Taker
We want to build a note taking app the satisfies the following user stories

- As a user I would like to write a plain text note that will store in my browsers `localstorage`
- As a user I would like to see a list of all my previously written notes
- As a user I would like to delete notes

This excerise is meant to be flexible so feel free to take it on in anyway you see fit, but we expect
to be delivered an HTML file, a JS file and a CSS file.

Take as much time as you feel you need but keep in mind we are looking for an understanding of technical
concepts, not for a pixel perfect application.

Good luck!
