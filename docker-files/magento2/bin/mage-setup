#!/bin/bash
echo "Initializing setup..."

if [ -f /var/www/html/app/etc/config.php ] || [ -f /var/www/html/app/etc/env.php ]; then
  echo "It appears Magento is already installed (app/etc/config.php or app/etc/env.php exist). Exiting setup..."

  # Magent is already installed, just run the server now
  set -e

  # Apache gets grumpy about PID files pre-existing
  rm -f /var/run/apache2/apache2.pid

  exec apache2 -DFOREGROUND
fi

echo "Running Magento 2 setup script..."
/usr/local/bin/php -d memory_limit=2G /var/www/html/bin/magento setup:install \
  --db-host=$M2SETUP_DB_HOST \
  --db-name=$M2SETUP_DB_NAME \
  --db-user=$M2SETUP_DB_USER \
  --db-password=$M2SETUP_DB_PASSWORD \
  --base-url=$M2SETUP_BASE_URL \
  --backend-frontname=$M2SETUP_ADMIN_URI \
  --admin-firstname=$M2SETUP_ADMIN_FIRSTNAME \
  --admin-lastname=$M2SETUP_ADMIN_LASTNAME \
  --admin-email=$M2SETUP_ADMIN_EMAIL \
  --admin-user=$M2SETUP_ADMIN_USER \
  --admin-password=$M2SETUP_ADMIN_PASSWORD

echo "Disabling maintenance mode..."
/usr/local/bin/php -d memory_limit=2G /var/www/html/bin/magento maintenance:disable

echo "Reindexing all indexes..."
/usr/local/bin/php -d memory_limit=2G /var/www/html/bin/magento indexer:reindex

echo "Deploying static view files..."
/usr/local/bin/php -d memory_limit=2G /var/www/html/bin/magento setup:static-content:deploy

echo "Applying ownership & proper permissions..."
sed -i -e "s/Symlink/Copy/g" /var/www/html/app/etc/di.xml
chown -Rf www-data:www-data /var/www/html
chmod -Rf 755 /var/www/html/pub

echo "The setup script has completed execution."

# Magento installation completed, run the server
set -e

# Apache gets grumpy about PID files pre-existing
rm -f /var/run/apache2/apache2.pid

exec apache2 -DFOREGROUND
