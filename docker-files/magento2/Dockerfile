FROM nimmis/apache-php7

### Setup server
RUN requirements="bzip2 libpng12-dev libjpeg-dev libjpeg62-turbo libmcrypt4 libmcrypt-dev libcurl3-dev libxml2-dev libxslt-dev libicu-dev libicu52" \
    && apt-get update && apt-get install -y $requirements && rm -rf /var/lib/apt/lists/* \
    && docker-php-ext-install pdo_mysql \
    && docker-php-ext-configure gd --with-jpeg-dir=/usr/lib \
    && docker-php-ext-install gd \
    && docker-php-ext-install mcrypt \
    && docker-php-ext-install mbstring \
    && docker-php-ext-install soap \
    && docker-php-ext-install xsl \
    && docker-php-ext-install intl \
    && requirementsToRemove="libpng12-dev libjpeg-dev libmcrypt-dev libcurl3-dev libxml2-dev libxslt-dev libicu-dev" \
    && apt-get purge --auto-remove -y $requirementsToRemove

RUN curl -sSL https://getcomposer.org/composer.phar -o /usr/bin/composer \
    && chmod +x /usr/bin/composer \
    && apt-get update && apt-get install -y zlib1g-dev git vim && rm -rf /var/lib/apt/lists/* \
    && docker-php-ext-install zip \
    && apt-get purge -y --auto-remove zlib1g-dev \
    && composer selfupdate

WORKDIR /var/www

RUN usermod -u 1000 www-data
RUN chown -R www-data:www-data /var/www
RUN a2enmod rewrite
RUN a2enmod headers
RUN apache2ctl graceful

# Copy config files
COPY ./conf/apache2.conf /etc/apache2/apache2.conf
COPY ./conf/php.ini /usr/local/etc/php/php.ini

### Setup Magento
# Copy archive over to container
COPY ./archive/Magento-CE-2.0.4.tar.bz2 /magento2.tar.bz2

# Extract fresh copy of Magento 2
RUN tar -C /var/www/html -jxvf /magento2.tar.bz2

# Copy .htaccess with custom headers
COPY ./conf/.htaccess /var/www/html/.htaccess

# Change permissions
WORKDIR /var/www/html
RUN chown -R www-data:www-data .
RUN find . -type d -exec chmod 770 {} \; && find . -type f -exec chmod 660 {} \;
RUN chmod u+x ./bin/magento
RUN chmod -R 755 ./app/etc var pub

# Copy install script(s)
COPY ./bin /var/bin
RUN chown -Rf www-data:www-data /var/bin
RUN chmod -Rf u+x /var/bin

WORKDIR /var/www/html
CMD ["/var/bin/mage-setup"]
