# Skyrocket Docker Files

This repo is specifically designed to host a myriad of dockerfiles that Skyrocket
uses.

These dockerfiles are JUST environments, not full application images.
